from setuptools import setup

setup(
    name="tljh-drawdownsolutions",
    license="3-clause BSD",
    author="DentonGentry",
    version="0.2",
    entry_points={"tljh": ["drawdownsolutions = tljh_drawdownsolutions"]},
    py_modules=["tljh_drawdownsolutions"],
)
