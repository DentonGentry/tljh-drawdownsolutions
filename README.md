# Drawdown Solutions Plugin for The Littlest JupyterHub

A plugin for [The Littlest JupyterHub](https://z2jh.jupyter.org)
that installs the [Drawdown Solutions stack](https://gitlab.com/codeearth/drawdown).
